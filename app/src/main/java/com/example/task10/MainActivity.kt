package com.example.task10

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnGoToInfoActivity = findViewById<Button>(R.id.btnGoToInfoActivity)

        val etSurname = findViewById<EditText>(R.id.etSurname)
        val etName = findViewById<EditText>(R.id.etName)
        val etPatronymic = findViewById<EditText>(R.id.etPatronymic)
        val etAge = findViewById<EditText>(R.id.etAge)
        val etHobby = findViewById<EditText>(R.id.etHobby)



            btnGoToInfoActivity.setOnClickListener {
                Intent(this, InfoActivity::class.java).also {
                    if (etSurname.text.toString() == "" ||
                        etName.text.toString() == "" ||
                        etPatronymic.text.toString() == "" ||
                        etAge.text.toString() == "" ||
                        etHobby.text.toString() == ""){
                        Toast.makeText(this, "Заполните все поля ввода", Toast.LENGTH_LONG).show()
                    }
                    else if (etAge.text.toString().toInt() in 1..120) {
                    it.putExtra("Surname", etSurname.text.toString())
                    it.putExtra("Name", etName.text.toString())
                    it.putExtra("Patronymic", etPatronymic.text.toString())
                    it.putExtra("Age", etAge.text.toString())
                    it.putExtra("Hobby", etHobby.text.toString())

                    startActivity(it)
                    }
                    else{
                        Toast.makeText(this, "Ваш возраст не реален, введите значение из диапазона от 1 до 120", Toast.LENGTH_LONG).show()
                    }
                }
            }


    }
}